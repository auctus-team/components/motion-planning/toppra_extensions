#include <pid/data_logger.h>
#include <toppra/path_tracking.h>

#include <chrono>
#include <cstdio>
#include <iostream>

using namespace std;
using namespace Eigen;

int main() {
  std::vector<VectorXd> waypoints;
  VectorXd waypoint(6);
  waypoint << 1, 6, 0, 3, 10, 5;
  waypoints.push_back(waypoint);
  waypoint << 5, 3, 8, 2, 0, 0;
  waypoints.push_back(waypoint);
  waypoint << 1, 6, 9, 17, 6, 4;
  waypoints.push_back(waypoint);

  std::cout << "#waypoints: " << waypoints.size() << '\n';

  VectorXd max_acceleration(6);
  max_acceleration.setOnes();
  VectorXd max_velocity(6);
  max_velocity.setOnes();
  VectorXd current_position = Eigen::VectorXd::Zero(6);

  const double time_step = 0.005;
  toppra::PathTracking path_tracking(
      waypoints, max_velocity, max_acceleration, time_step,
      &current_position,           // actual position to monitor
      VectorXd::Constant(6, 0.05), // stop deviation
      VectorXd::Constant(6, 0.01)  // resume deviation
  );

  double time = 0;
  Eigen::VectorXd position(6), velocity(6), acceleration(6);
  pid::DataLogger logger("/tmp", time, pid::DataLogger::CreateGnuplotFiles);
  logger.log("current_position", current_position.data(),
             current_position.size());
  logger.log("target_position", position.data(), position.size());
  logger.log("target_velocity", velocity.data(), velocity.size());
  logger.log("target_acceleration", acceleration.data(), acceleration.size());

  position = current_position.eval();

  double last_print_time = -1;
  const auto &trajectory = path_tracking.trajectory();
  if (trajectory.isValid()) {
    cout << "Trajectory duration: " << trajectory.getDuration() << " s" << endl
         << endl;
    cout << "Time      Position                  Velocity" << endl;
    const double perturbation_time = trajectory.getDuration() / 3.;
    const double perturbation_duration = 2.;
    while (path_tracking.state() != toppra::PathTracking::State::Completed) {
      current_position = position;
      if (time > perturbation_time and
          time < perturbation_time + perturbation_duration) {
        current_position +=
            VectorXd::Constant(6, 0.1 * std::sin(M_PI / perturbation_duration *
                                                 (time - perturbation_time)));
      }
      path_tracking();
      position = path_tracking.getPosition().eval();
      velocity = path_tracking.getVelocity().eval();
      acceleration = path_tracking.getAcceleration().eval();
      if (time - last_print_time > 0.1) {
        last_print_time = time;
        printf("[%s] %6.4f   %7.4f %7.4f %7.4f   %7.4f %7.4f %7.4f\n",
               path_tracking.state() == toppra::PathTracking::State::Running
                   ? "Running"
                   : "Stopped",
               time, position[0], position[1], position[2], position[3],
               position[4], position[5]);
      }
      time += time_step;
      logger();
    }
  } else {
    cout << "Trajectory generation failed." << endl;
  }
}
