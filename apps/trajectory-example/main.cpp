

#include <pid/data_logger.h>
#include <toppra/constrained_trajectory.h>

#include <chrono>
#include <cstdio>
#include <iostream>

using namespace std;
using namespace Eigen;

int main() {
  std::vector<VectorXd> waypoints;
  VectorXd waypoint(6);
  waypoint.setZero();
  waypoints.push_back(waypoint);
  waypoint << 1, 6, 0, 3, 10, 5;
  waypoints.push_back(waypoint);
  waypoint << 1, 6, 0, 3, 10, 5;
  waypoints.push_back(waypoint);
  waypoint << 5, 3, 8, 2, 0, 0;
  waypoints.push_back(waypoint);
  waypoint << 1, 6, 9, 17, 6, 4;
  waypoints.push_back(waypoint);

  std::cout << "#waypoints: " << waypoints.size() << '\n';

  VectorXd max_acceleration(6);
  max_acceleration.setOnes();
  VectorXd max_velocity(6);
  max_velocity.setOnes();

  auto t_start = std::chrono::high_resolution_clock::now();

  const double time_step = 0.01;
  toppra::ConstrainedTrajectory trajectory(waypoints, max_velocity,
                                           max_acceleration, time_step);

  auto t_end = std::chrono::high_resolution_clock::now();

  std::cout << "Generation took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(t_end -
                                                                     t_start)
                   .count()
            << "ms\n";

  double time = 0;
  Eigen::VectorXd position(6), velocity(6), acceleration(6);
  pid::DataLogger logger("/tmp", time, pid::DataLogger::CreateGnuplotFiles);
  logger.log("position", position.data(), position.size());
  logger.log("velocity", velocity.data(), velocity.size());
  logger.log("acceleration", acceleration.data(), acceleration.size());

  double last_print_time = -1;
  if (trajectory.isValid()) {
    double duration = trajectory.getDuration();
    cout << "Trajectory duration: " << duration << " s" << endl << endl;
    cout << "Time      Position" << endl;
    while (time <= duration) {
      position = trajectory.getPosition(time).eval();
      velocity = trajectory.getVelocity(time).eval();
      acceleration = trajectory.getAcceleration(time).eval();
      if (time - last_print_time > 0.1) {
        last_print_time = time;
        printf("%6.4f   %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f\n", time,
               position[0], position[1], position[2], position[3], position[4],
               position[5]);
      }
      time += time_step;
      logger();
    }
  } else {
    cout << "Trajectory generation failed." << endl;
  }
}
