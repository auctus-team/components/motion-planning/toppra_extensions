#pragma once

#include "toppra/algorithm/toppra.hpp"
#include "toppra/constraint/linear_joint_acceleration.hpp"
#include "toppra/constraint/linear_joint_velocity.hpp"
#include "toppra/geometric_path/piecewise_poly_path.hpp"
#include <toppra_extensions/toppra/parametrizer/const_accel_ext.h>
#include "toppra/toppra.hpp"

namespace toppra {

class ConstrainedTrajectory {
public:
  // Construct the trajectory
  ConstrainedTrajectory(const std::vector<Eigen::VectorXd> &points,
                        const Eigen::VectorXd &max_velocity,
                        const Eigen::VectorXd &max_acceleration,
                        double time_step);

  const bool &isValid() const { return is_valid_; };

  // Returns the optimal duration of the trajectory
  double getDuration() const;

  const Eigen::VectorXd &getMaxVelocity() const { return max_velocity_; }

  const Eigen::VectorXd &getMaxAcceleration() const {
    return max_acceleration_;
  }

  double getTimeStep() const { return time_step_; }

  std::vector<double> getWaypointTimes() const;

  // Return the position/configuration, velocity or acceleration vector of the
  // robot for a given point in time within the trajectory.
  Eigen::VectorXd getPosition(double time) const;
  Eigen::VectorXd getVelocity(double time) const;
  Eigen::VectorXd getAcceleration(double time) const;

  // Return the vector of time points corresponding to each waypoint
  // std::vector<double> getWaypointTimes() const;

private:
  BoundaryCond makeBoundaryCond(const int order,
                                const std::vector<value_type> &values);

  void createPath();
  void createConstraints();
  void createAlgo();
  void createParametrizer();

  Vectors waypoints_;
  Eigen::VectorXd max_velocity_;
  Eigen::VectorXd max_acceleration_;
  double time_step_;
  bool is_valid_;

  toppra::GeometricPathPtr path_;
  toppra::LinearConstraintPtrs constraints_;
  toppra::PathParametrizationAlgorithmPtr algo_;
  std::shared_ptr<toppra::parametrizer::ConstAccelExt>
      const_accel_parametrizer_;
};

} // namespace toppra