#pragma once

#include <toppra_extensions/toppra/constrained_trajectory.h>

namespace toppra
{

class PathTracking
{
public:
    enum class State
    {
        Running,
        Stopped,
        Completed
    };

    // Use the current position as the initial waypoint
    PathTracking(const std::vector<Eigen::VectorXd>& waypoints,
                 const Eigen::VectorXd& max_velocity,
                 const Eigen::VectorXd& max_acceleration, double time_step,
                 const Eigen::VectorXd* current_position,
                 const Eigen::VectorXd& stop_deviation,
                 const Eigen::VectorXd& resume_deviation,
                 const Eigen::VectorXd* current_velocity = nullptr);

    const State& process();

    const State& operator()()
    {
        return process();
    }

    // Same as Trajectory::getPosition(time) but evaluated the the current time
    Eigen::VectorXd getPosition() const;

    // Same as Trajectory::getVelocity(time) but evaluated the the current time
    Eigen::VectorXd getVelocity() const;

    // Same as Trajectory::getAcceleration(time) but evaluated the the current
    // time
    Eigen::VectorXd getAcceleration() const;

    const ConstrainedTrajectory& trajectory() const
    {
        return trajectory_;
    }

    const Eigen::VectorXd& stopDeviation() const
    {
        return stop_deviation_;
    }

    Eigen::VectorXd& stopDeviation()
    {
        return stop_deviation_;
    }

    const Eigen::VectorXd& resumeDeviation() const
    {
        return resume_deviation_;
    }

    Eigen::VectorXd& resumeDeviation()
    {
        return resume_deviation_;
    }

    const State& state() const
    {
        return state_;
    }

    const double& currentTime() const
    {
        return current_time_;
    }

    const auto& currentPointIndex() const
    {
        return current_point_index_;
    }

private:
    void recomputeTrajectory();

    Eigen::VectorXd getCurrentVelocity() const;

    ConstrainedTrajectory trajectory_;
    std::vector<Eigen::VectorXd> waypoints_;
    State state_{State::Running};
    double current_time_{0};
    const Eigen::VectorXd* current_position_{nullptr};
    const Eigen::VectorXd* current_velocity_{nullptr};
    Eigen::VectorXd stop_deviation_;
    Eigen::VectorXd resume_deviation_;
    Eigen::VectorXd previous_position_;

    std::vector<double> waypoint_times_;
    size_t current_point_index_{0};
};

} // namespace toppra