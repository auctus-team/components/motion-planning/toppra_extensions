#include <toppra_extensions/toppra/path_tracking.h>

#include <iostream>

namespace
{
std::vector<Eigen::VectorXd>
addInFront(const Eigen::VectorXd& value,
           const std::vector<Eigen::VectorXd>& vector)
{
    std::vector<Eigen::VectorXd> res;
    res.reserve(vector.size() + 1);
    res.emplace_back(value);
    std::copy(vector.begin(), vector.end(), std::back_inserter(res));
    return res;
}
} // namespace

namespace toppra
{

PathTracking::PathTracking(const std::vector<Eigen::VectorXd>& waypoints,
                           const Eigen::VectorXd& max_velocity,
                           const Eigen::VectorXd& max_acceleration,
                           double time_step,
                           const Eigen::VectorXd* current_position,
                           const Eigen::VectorXd& stop_deviation,
                           const Eigen::VectorXd& resume_deviation,
                           const Eigen::VectorXd* current_velocity)
    : trajectory_{::addInFront(*current_position, waypoints), max_velocity,
                  max_acceleration, time_step},
      waypoints_{::addInFront(*current_position, waypoints)},
      current_position_{current_position},
      current_velocity_{current_velocity},
      stop_deviation_{stop_deviation},
      resume_deviation_{resume_deviation},
      previous_position_{*current_position}
{
    waypoint_times_ = trajectory().getWaypointTimes();
}

Eigen::VectorXd PathTracking::getPosition() const
{
    if (state_ == State::Running)
        return trajectory_.getPosition(current_time_);
    else
        return *current_position_;
}

Eigen::VectorXd PathTracking::getVelocity() const
{
    if (state_ == State::Running)
        return trajectory_.getVelocity(current_time_);
    else
        return Eigen::VectorXd::Zero(current_position_->size());
}

Eigen::VectorXd PathTracking::getAcceleration() const
{
    if (state_ == State::Running)
        return trajectory_.getAcceleration(current_time_);
    else
        return Eigen::VectorXd::Zero(current_position_->size());
}

Eigen::VectorXd PathTracking::getCurrentVelocity() const
{
    if (current_velocity_)
        return *current_velocity_;
    else
        return ((*current_position_ - previous_position_) /
                trajectory_.getTimeStep())
            .eval();
}

const PathTracking::State& PathTracking::process()
{

    switch (state_)
    {
    case State::Running:
    {
        bool stop{false};
        const auto target_position = getPosition();
        auto error = (target_position - *current_position_).cwiseAbs().eval();
        for (Eigen::Index i = 0; i < error.size(); i++)
        {
            if (error(i) >= stopDeviation()(i))
            {
                stop = true;
                break;
            }
        }
        if (stop)
        {
            state_ = State::Stopped;
        }
    }
    break;
    case State::Stopped:
    {
        bool resume{true};
        auto error = getCurrentVelocity().cwiseAbs().eval();
        for (Eigen::Index i = 0; i < error.size(); i++)
        {
            if (error(i) > resumeDeviation()(i))
            {
                resume = false;
                break;
            }
        }
        if (resume)
        {
            state_ = State::Running;
            recomputeTrajectory();
        }
    }
    break;
    case State::Completed:
    {
        std::cout << "TOPPRA path tracking completed, doing nothing\n";
    }
    break;
    }

    if (state_ == State::Running)
    {
        if (current_time_ == trajectory().getDuration())
        {
            state_ = State::Completed;
        }
        else
        {
            auto previous_time = current_time_;
            current_time_ += trajectory().getTimeStep();
            if (current_time_ > trajectory().getDuration())
                current_time_ = trajectory().getDuration();

            if (waypoint_times_[0] <= current_time_)
            {
                waypoint_times_.erase(waypoint_times_.begin());
                waypoints_.erase(waypoints_.begin());
                current_point_index_++;
            }
        }
    }

    previous_position_ = *current_position_;

    return state_;
}

void PathTracking::recomputeTrajectory()
{
    waypoints_.insert(waypoints_.begin(), *current_position_);

    trajectory_ = ConstrainedTrajectory{waypoints_, trajectory().getMaxVelocity(),
                                        trajectory().getMaxAcceleration(),
                                        trajectory().getTimeStep()};

    waypoint_times_ = trajectory().getWaypointTimes();

    current_time_ = 0;
    current_point_index_--;
}

} // namespace toppra