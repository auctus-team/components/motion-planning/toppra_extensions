#include <toppra_extensions/toppra/constrained_trajectory.h>

using namespace toppra;

ConstrainedTrajectory::ConstrainedTrajectory(
    const std::vector<Eigen::VectorXd> &waypoints,
    const Eigen::VectorXd &max_velocity,
    const Eigen::VectorXd &max_acceleration, double time_step)
    : max_velocity_(max_velocity), max_acceleration_(max_acceleration),
      time_step_(time_step), is_valid_(true) {

  for (const auto &waypoint : waypoints)
    waypoints_.push_back(waypoint);

  createPath();
  createConstraints();
  createAlgo();
  createParametrizer();
}

BoundaryCond ConstrainedTrajectory::makeBoundaryCond(
    const int order, const std::vector<toppra::value_type> &values) {
  BoundaryCond cond;
  cond.order = order;
  cond.values.resize(values.size());
  for (std::size_t i = 0; i < values.size(); i++)
    cond.values(i) = values[i];
  return cond;
}

void ConstrainedTrajectory::createPath() {
  toppra::Vector times(waypoints_.size());
  for (auto i = 0; i < waypoints_.size(); ++i) {
    times(i) = i;
  }
  std::vector<toppra::value_type> bound_cond_values(waypoints_[0].size());
  for (int i =0 ; i< bound_cond_values.size(); ++i)
    bound_cond_values[i] = 0.0;
  toppra::BoundaryCond bc = makeBoundaryCond(2, bound_cond_values);
  std::array<toppra::BoundaryCond, 2> bc_type{bc, bc};

  path_ =
      std::make_shared<toppra::PiecewisePolyPath>(waypoints_, times, bc_type);
}
void ConstrainedTrajectory::createConstraints() {
  toppra::LinearConstraintPtr ljv, lja;
  ljv = std::make_shared<toppra::constraint::LinearJointVelocity>(
      -max_velocity_, max_velocity_);
  constraints_.push_back(ljv);
  lja = std::make_shared<toppra::constraint::LinearJointAcceleration>(
      -max_acceleration_, max_acceleration_);
  lja->discretizationType(toppra::DiscretizationType::Interpolation);
  constraints_.push_back(lja);
}
void ConstrainedTrajectory::createAlgo() {
  algo_ = std::make_shared<toppra::algorithm::TOPPRA>(constraints_, path_);
  toppra::ReturnCode rc = algo_->computePathParametrization(0, 0);
  is_valid_ &= (rc == toppra::ReturnCode::OK);
  if (not is_valid_)
    std::cerr << "TOPPRA algorithm failed with error code" << int(rc)
              << std::endl;
}

void ConstrainedTrajectory::createParametrizer() {
  toppra::ParametrizationData pd = algo_->getParameterizationData();

  toppra::Vector gridpoints =
      pd.gridpoints; // Grid-points used for solving the discretized problem.
  toppra::Vector vsquared =
      pd.parametrization; // Output parametrization (squared path velocity)
  const_accel_parametrizer_ =
      std::make_shared<toppra::parametrizer::ConstAccelExt>(path_, gridpoints,
                                                            vsquared);
  bool ca_validate = const_accel_parametrizer_->validate();
  if (not ca_validate)
    std::cerr << "TOPPRA failed to generate constant acceleration parametrizer"
              << std::endl;
  is_valid_ &= ca_validate;
}

std::vector<double> ConstrainedTrajectory::getWaypointTimes() const {
  std::vector<double> times(waypoints_.size());
  const auto &gridpoints = algo_->getParameterizationData().gridpoints;
  for (auto i = 0; i < waypoints_.size(); ++i) {
    auto it = std::lower_bound(gridpoints.data(),
                               gridpoints.data() + gridpoints.size(), i);
    if (it != (gridpoints.data() + gridpoints.size())) {
      int index = it - gridpoints.data();
      times[i] = const_accel_parametrizer_->getTimeAtGridpoint(index);
    } else {
      throw std::runtime_error("Impossible to get waypoint times!");
    }
  }

  return times;
}

double ConstrainedTrajectory::getDuration() const {
  return const_accel_parametrizer_->pathInterval()(1);
}

Eigen::VectorXd ConstrainedTrajectory::getPosition(double time) const {
  return const_accel_parametrizer_->eval_single(time, 0);
}
Eigen::VectorXd ConstrainedTrajectory::getVelocity(double time) const {
  return const_accel_parametrizer_->eval_single(time, 1);
}
Eigen::VectorXd ConstrainedTrajectory::getAcceleration(double time) const {
  return const_accel_parametrizer_->eval_single(time, 2);
}
