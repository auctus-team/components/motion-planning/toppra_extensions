# [](https://gite.lirmm.fr/rpc/control/toppra-extensions/compare/v0.0.0...v) (2021-03-25)


### Bug Fixes

* removed package.json ([0dabdb5](https://gite.lirmm.fr/rpc/control/toppra-extensions/commits/0dabdb5f7512122f55bc4e87f6902fd85b2f3de3))
* updated addressa after repository migration ([956dc29](https://gite.lirmm.fr/rpc/control/toppra-extensions/commits/956dc299fff68ef1bab167f0e42a6536f5ba0eea))
* wrong public address ([d9b2e6c](https://gite.lirmm.fr/rpc/control/toppra-extensions/commits/d9b2e6ce98c9155a7d8ebf4d755e7292eb52cc60))


### Features

* added trajectory and path tracking + examples ([f9a0b15](https://gite.lirmm.fr/rpc/control/toppra-extensions/commits/f9a0b158eb15e0528fd502a8c37cdb14a7268cc9))



# 0.0.0 (2021-02-17)



