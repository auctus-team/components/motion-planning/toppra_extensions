
# toppra_extensions

This is a fork from https://gite.lirmm.fr/rpc/control/toppra-extensions to use toppra and toppra-extensions in a catkin environment

Overview
=========

C++ library extending the toppra library



The license that applies to the whole package content is **CeCILL**. Please look at the license.txt file at the root of this repository.

Installation and Usage
=======================

The detailed procedures for installing the toppra-extensions package and for using its components is available in this [site][package_site]. It is based on a CMake based build and deployment system called [PID](http://pid.lirmm.net/pid-framework/pages/install.html). Just follow and read the links to understand how to install, use and call its API and/or applications.

For a quick installation:

## Installing the project into an existing PID workspace

To get last version :
 ```
cd <path to pid workspace>
pid deploy package=toppra-extensions
```

To get a specific version of the package :
 ```
cd <path to pid workspace>
pid deploy package=toppra-extensions version=<version number>
```

## Standalone install
 ```
git clone https://gite.lirmm.fr/rpc/control/toppra-extensions.git
cd toppra-extensions
```

Then run the adequate install script depending on your system. For instance on linux:
```
sh share/install/standalone_install.sh
```

The pkg-config tool can be used to get all links and compilation flags for the libraries defined in the project.

To let pkg-config know these libraries, read the output of the install_script and apply the given command to configure the PKG_CONFIG_PATH.

For instance on linux do:
```
export PKG_CONFIG_PATH=<given path>:$PKG_CONFIG_PATH
```

Then, to get compilation flags run:

```
pkg-config --static --cflags toppra-extensions_<name of library>
```

```
pkg-config --variable=c_standard toppra-extensions_<name of library>
```

```
pkg-config --variable=cxx_standard toppra-extensions_<name of library>
```

To get linker flags run:

```
pkg-config --static --libs toppra-extensions_<name of library>
```


About authors
=====================

toppra-extensions has been developped by following authors: 
+ Sonny Tarbouriech (LIRMM)

Please contact Sonny Tarbouriech - LIRMM for more information or questions.


[package_site]: https://rpc.lirmm.net/rpc-framework/packages/toppra-extensions "toppra-extensions package"


